ALTER TABLE meets ADD COLUMN location_name VARCHAR NOT NULL;
ALTER TABLE meets ADD COLUMN location_address VARCHAR NOT NULL;
ALTER TABLE meets DROP COLUMN location_description;

ALTER TABLE meets ADD COLUMN location_description VARCHAR NOT NULL;
ALTER TABLE meets DROP COLUMN location_name;
ALTER TABLE meets DROP COLUMN location_address;

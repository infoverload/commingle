CREATE TABLE meets (
  id SERIAL PRIMARY KEY,
  uuid VARCHAR NOT NULL,
  title VARCHAR NOT NULL,
  location_place_id VARCHAR NOT NULL,
  location_description VARCHAR NOT NULL,
  starts TIMESTAMPTZ NOT NULL,
  ends TIMESTAMPTZ NOT NULL,
  description VARCHAR NOT NULL,
  /* TODO: Make into many-to-many relation */
  hosts VARCHAR NOT NULL,
  attendee_limit INTEGER,
  added TIMESTAMPTZ NOT NULL
)

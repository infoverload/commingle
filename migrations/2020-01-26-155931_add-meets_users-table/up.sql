CREATE TABLE meets_users (
  id SERIAL PRIMARY KEY,
  meet_id INTEGER NOT NULL REFERENCES meets,
  user_id INTEGER NOT NULL REFERENCES users,
  UNIQUE(meet_id, user_id)
)

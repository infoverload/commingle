#![feature(proc_macro_hygiene)]

#[macro_use]
extern crate maud;
#[macro_use]
extern crate diesel;

use serde::Serialize;
use std::collections::HashMap;
use std::env;
use std::sync::Arc;
use warp::Filter;

mod api;
mod auth;
mod db;
mod google_maps;
mod svg;
mod viewmodels;

use api::*;
use auth::{auth, try_auth};
use db::*;
use svg::*;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct AppConfig {
    pub log_level: String,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct InitialState {
    pub logged_in_user: Option<User>,
    pub version: String,
}

/// Handler for site index route. Renders HTML index document.
async fn get_index(
    session: Option<User>,
    svgs: Arc<Vec<Svg>>,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Getting index");
    match session.as_ref() {
        Some(ref user) => {
            println!("User is authenticated: {}", user.username);
        }
        None => {
            println!("User is not authenticated");
        }
    };

    let app_config = AppConfig {
        log_level: "debug".into(),
    };
    let initial_state = InitialState {
        logged_in_user: session.clone(),
        version: env::var("CARGO_PKG_VERSION").expect("Failed to get $CARGO_PKG_VERSION"),
    };
    let app_config_str = serde_json::to_string(&app_config).expect("Serialize app config");
    let initial_state_str =
        serde_json::to_string(&initial_state).expect("Failed to serialize initial state");
    println!("Sending initial state to client: {}", initial_state_str);

    // Render HTML using Maud templating engine
    let doc = html! {
        head {
            meta charset="utf-8";
            meta name="viewport" content="width=device-width, initial-scale=1";
            meta property="og:type" content="website";

            title {"Commingle"}

            link rel="stylesheet" href="https://unpkg.com/tachyons@4.10.0/css/tachyons.min.css";
            link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat|Proza+Libre";
            link rel="stylesheet" href="/bundle.css";

            style {
                "#container {visibility: hidden};"
            }

            script type="text/javascript" src="/bundle.js" defer="true" {}
        }
        body."w-100" {
            svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" {
                defs {
                    @for svg in svgs.iter() {
                        (svg)
                    }
                }
            }
            #app-config data-json=(app_config_str) {}
            #initial-state data-json=(initial_state_str) {}
            #container {}
        }
    };
    Ok(warp::reply::html(doc.into_string()))
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let svgs = load_svgs()?;

    let database = Database::new();
    let database_clone = database.clone();
    let database_filter = warp::any().map(move || database_clone.clone());

    let json_body_limit = 1024 * 16;

    let homepage =
        warp::get()
            .and(try_auth(database.clone()))
            .and_then(move |session: Option<User>| {
                println!("Getting index, logged in: {}", session.is_some());
                get_index(session, svgs.clone())
            });
    let js_bundle = warp::get()
        .and(warp::path("bundle.js"))
        .and(warp::path::end())
        .and(warp::fs::file("./dist/bundle.js"));
    let js_map = warp::get()
        .and(warp::path("bundle.js.map"))
        .and(warp::path::end())
        .and(warp::fs::file("./dist/bundle.js.map"));
    let css_bundle = warp::get()
        .and(warp::path("bundle.css"))
        .and(warp::path::end())
        .and(warp::fs::file("./dist/bundle.css"));
    // TODO: Figure out why the request is re-routed to the homepage route after this executes.
    let api_not_found = warp::path("api")
        .and(warp::any())
        .and_then(any_api_not_found);
    let api_login = warp::post()
        .and(warp::path("api"))
        .and(warp::path("login"))
        .and(warp::path::end())
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(database_filter.clone())
        .and_then(post_api_login);
    let api_logout = warp::post()
        .and(warp::path("api"))
        .and(warp::path("logout"))
        .and(warp::path::end())
        .and_then(post_api_logout);
    let api_signup = warp::post()
        .and(warp::path("api"))
        .and(warp::path("accounts"))
        .and(warp::path::end())
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(database_filter.clone())
        .and_then(post_api_signup);
    let api_get_user = warp::get()
        .and(warp::path("api"))
        .and(warp::path("users"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(database_filter.clone())
        .and_then(get_api_user);
    let api_get_user_groups = warp::get()
        .and(warp::path("api"))
        .and(warp::path("users"))
        .and(warp::path::param::<String>())
        .and(warp::path("groups"))
        .and(warp::path::end())
        .and(database_filter.clone())
        .and_then(get_api_user_groups);
    let api_get_user_upcoming_meets = warp::get()
        .and(warp::path("api"))
        .and(warp::path("users"))
        .and(warp::path::param::<String>())
        .and(warp::path("upcoming-meets"))
        .and(warp::path::end())
        .and(database_filter.clone())
        .and_then(get_api_user_upcoming_meets);
    let api_get_user_past_meets = warp::get()
        .and(warp::path("api"))
        .and(warp::path("users"))
        .and(warp::path::param::<String>())
        .and(warp::path("past-meets"))
        .and(warp::path::end())
        .and(database_filter.clone())
        .and_then(get_api_user_past_meets);
    let api_get_group = warp::get()
        .and(warp::path("api"))
        .and(warp::path("groups"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(database_filter.clone())
        .and_then(get_api_group);
    let api_post_group = warp::post()
        .and(auth(database.clone()))
        .and(warp::path("api"))
        .and(warp::path("groups"))
        .and(warp::path::end())
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(database_filter.clone())
        .and_then(post_api_group);
    let api_get_place_predictions = warp::get()
        .and(warp::path("api"))
        .and(warp::path("place-predictions"))
        .and(warp::path::end())
        .and(warp::query::query::<HashMap<String, String>>())
        .and_then(get_api_place_predictions);
    let api_get_map_image_url = warp::get()
        .and(warp::path("api"))
        .and(warp::path("place-details"))
        .and(warp::path::end())
        .and(warp::query::query::<HashMap<String, String>>())
        .and_then(get_api_place_details);
    let api_get_meet = warp::get()
        .and(warp::path("api"))
        .and(warp::path("meets"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(database_filter.clone())
        .and_then(get_api_meet);
    let api_post_meet = warp::post()
        .and(auth(database.clone()))
        .and(warp::path("api"))
        .and(warp::path("meets"))
        .and(warp::path::end())
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(database_filter.clone())
        .and_then(post_api_meet);
    let api_post_meet_attendee = warp::post()
        .and(auth(database.clone()))
        .and(warp::path("api"))
        .and(warp::path("meets"))
        .and(warp::path::param::<String>())
        .and(warp::path("attendees"))
        .and(warp::path::end())
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(database_filter.clone())
        .and_then(post_api_meet_attendee);
    let api_delete_meet_attendee = warp::delete()
        .and(auth(database.clone()))
        .and(warp::path("api"))
        .and(warp::path("meets"))
        .and(warp::path::param::<String>())
        .and(warp::path("attendees"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(database_filter.clone())
        .and_then(delete_api_meet_attendee);

    let routes = js_bundle
        .or(js_map)
        .or(css_bundle)
        .or(api_login)
        .or(api_logout)
        .or(api_signup)
        .or(api_get_user)
        .or(api_get_user_groups)
        .or(api_get_user_upcoming_meets)
        .or(api_get_user_past_meets)
        .or(api_get_group)
        .or(api_post_group)
        .or(api_get_place_predictions)
        .or(api_get_map_image_url)
        .or(api_post_meet)
        .or(api_get_meet)
        .or(api_post_meet_attendee)
        .or(api_delete_meet_attendee)
        .or(api_not_found)
        .or(homepage);

    let port = 8000;
    println!("Serving on localhost:{}", port);
    warp::serve(routes).run(([127, 0, 0, 1], 8000)).await;

    Ok(())
}

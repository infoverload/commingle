use hyper::Body;
use paseto::v2::local::{decrypt_paseto, local_paseto};
use serde::{Deserialize, Serialize};
use std::convert::Infallible;
use warp::http::header::SET_COOKIE;
use warp::{filters, reply, Filter};

use crate::db::{Database, User};

#[derive(Serialize, Deserialize)]
struct Session {
    id: i64,
}

/// Log user in by setting cookie.
pub fn log_user_in(user: &User) -> Result<reply::Response, warp::Rejection> {
    // TODO: Use a configured password
    let mut key_mut = Vec::from("YELLOW SUBMARINE, BLACK WIZARDRY".as_bytes());
    let state = Session {
        id: user.id.clone() as i64,
    };
    let serialized_state = serde_json::to_string(&state).expect("Failed to serialize state");
    let payload =
        local_paseto(&serialized_state, None, &mut key_mut).expect("Failed to encrypt payload");
    let max_age = chrono::Duration::days(30).num_seconds();
    // TODO: If not on localhost, set to "Secure;"
    let secure = "";
    let cookie = format!(
        "sid={}; Max-Age={}; SameSite=Strict; HttpOnly; Path=/;{}",
        payload, max_age, secure
    );

    let user_json = serde_json::to_string(&user).expect("Failed to encode user");
    let resp = warp::http::Response::builder()
        .header(SET_COOKIE, cookie)
        .header("Content-Type", "application/json")
        .body(Body::from(user_json))
        .expect("Failed to build response");
    Ok(resp)
}

/// Log user out by unsetting cookie.
pub fn log_user_out() -> Result<reply::Response, warp::Rejection> {
    // TODO: If not on localhost, set to "Secure;"
    let secure = "";
    let cookie = format!(
        "sid=; Max-Age=0; SameSite=Strict; HttpOnly; Path=/;{}",
        secure
    );
    let resp = warp::http::Response::builder()
        .header(SET_COOKIE, cookie)
        .body(Body::from(""))
        .expect("Failed to build response");
    Ok(resp)
}

/// Filter for trying to get user credentials from request.
/// TODO: Clear invalid cookies. In case we have changed our format, we have to get rid of old,
/// stale, cookies
pub fn try_auth(
    database: Database,
) -> impl Filter<Extract = (Option<User>,), Error = Infallible> + Clone {
    filters::cookie::optional("sid").map(move |opt: Option<String>| {
        println!("Optional cookie: {}", opt.is_some());
        opt.and_then(|token| {
            println!("Obtained token {}", token);
            let mut key = Vec::from("YELLOW SUBMARINE, BLACK WIZARDRY".as_bytes());
            match decrypt_paseto(&token, None, &mut key) {
                Ok(json) => match serde_json::from_str::<Session>(&json) {
                    Ok(state) => {
                        println!("Decoded session state: {}", state.id);
                        database.get_user(state.id)
                    }
                    Err(err) => {
                        println!("Failed to decode state from auth cookie: {}", err);
                        None
                    }
                },
                Err(err) => {
                    println!("Failed to decrypt session token: {}", err);
                    None
                }
            }
        })
    })
}

/// Filter for getting user credentials from request.
/// TODO: Clear invalid cookies. In case we have changed our format, we have to get rid of old,
/// stale, cookies
pub fn auth(database: Database) -> impl Filter<Extract = (User,), Error = warp::Rejection> + Clone {
    filters::cookie::cookie("sid").map(move |token: String| {
        println!("Obtained auth token {}", token);
        let mut key = Vec::from("YELLOW SUBMARINE, BLACK WIZARDRY".as_bytes());
        match decrypt_paseto(&token, None, &mut key) {
            Ok(json) => match serde_json::from_str::<Session>(&json) {
                Ok(state) => {
                    println!("Decoded session state: {}", state.id);
                    // TODO: Turn into rejection
                    database.get_user(state.id).expect("Non-existent user")
                }
                Err(err) => {
                    println!("Failed to decode state from auth cookie: {}", err);
                    // TODO: Return rejection
                    panic!("Invalid auth cookie");
                }
            },
            Err(err) => {
                println!("Failed to decrypt session token: {}", err);
                // TODO: Return rejection
                panic!("Invalid auth cookie");
            }
        }
    })
}

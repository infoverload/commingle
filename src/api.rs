use chrono::{DateTime, Duration, Utc};
use hyper::Body;
use md5::{Digest, Md5};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::ops::Add;
use uuid::Uuid;
use warp::http::status::StatusCode;

use super::viewmodels::*;
use crate::auth::*;
use crate::db::*;
use crate::google_maps;

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Signup {
    pub username: String,
    pub name: String,
    pub email_address: String,
    pub password: String,
    pub bio: Option<String>,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Login {
    pub username_or_email: String,
    pub password: String,
}

pub async fn any_api_not_found() -> Result<::hyper::StatusCode, warp::Rejection> {
    println!("Received request for non-existent API route");
    Err(warp::reject::not_found())
}

pub async fn post_api_signup(
    signup: Signup,
    database: Database,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received signup request, username: {}, email: {}",
        signup.username, signup.email_address,
    );
    let password_hash = bcrypt::hash(signup.password, bcrypt::DEFAULT_COST).expect("hash password");

    let user = NewUser::new(
        signup.username,
        signup.name,
        signup.email_address,
        password_hash,
        signup.bio,
    );
    let db_user = database.add_user(user);
    Ok(log_user_in(&db_user).expect("Failed to log user in"))
}

pub async fn post_api_login(
    login: Login,
    database: Database,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received login request, username/email: {}",
        login.username_or_email
    );
    match database.get_user_by_username_or_email(&login.username_or_email) {
        Some(user) => {
            println!("Found user in database");
            if bcrypt::verify(&login.password, &user.password_hash)
                .expect("Failed to verify password")
            {
                println!("User logged in");
                Ok(log_user_in(&user).expect("Failed to log user in"))
            } else {
                println!("User not authenticated");
                let resp = hyper::http::Response::builder()
                    .status(StatusCode::UNAUTHORIZED)
                    .body(Body::from(""))
                    .expect("Failed to build response");
                Ok(resp)
            }
        }
        None => {
            println!("Couldn't find user in database");
            let resp = hyper::http::Response::builder()
                .status(StatusCode::UNAUTHORIZED)
                .body(Body::from(""))
                .expect("Failed to build response");
            Ok(resp)
        }
    }
}

pub async fn post_api_logout() -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received logout request");
    log_user_out()
}

pub async fn get_api_user(
    username: String,
    database: Database,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to get user with username: {}", username);
    match database.get_user_by_username(&username) {
        Some(mut user) => {
            if user.avatar_url.is_none() {
                let mut hasher = Md5::new();
                hasher.input(user.email_address.to_lowercase());
                let email_hash = hasher.result();
                user.avatar_url = Some(format!(
                    "http://www.gravatar.com/avatar/{:x}?d=identicon&s=300",
                    email_hash
                ));
            }
            Ok(warp::reply::json(&user))
        }
        None => Err(warp::reject::not_found()),
    }
}

/// Handler for API route to get groups of a user.
pub async fn get_api_user_groups(
    username: String,
    database: Database,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to get groups of user '{}'", username);
    let groups = database.get_user_groups(&username);
    println!("User has {} group(s)", groups.len());
    Ok(warp::reply::json(&groups))
}

/// Handler for API route to get upcoming meets of a user.
pub async fn get_api_user_upcoming_meets(
    username: String,
    database: Database,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received request to get upcoming meets of user '{}'",
        username
    );
    let meets = database.get_user_upcoming_meets(&username);
    println!("User has {} upcoming meet(s)", meets.len());
    Ok(warp::reply::json(&meets))
}

/// Handler for API route to get past meets of a user.
pub async fn get_api_user_past_meets(
    username: String,
    database: Database,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to get past meets of user '{}'", username);
    let meets = database.get_user_past_meets(&username);
    println!("User has {} past meet(s)", meets.len());
    Ok(warp::reply::json(&meets))
}

/// Handler for API route to get group.
pub async fn get_api_group(
    path: String,
    database: Database,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to get group '{}'", path);
    match database.get_group_view_model_by_path(&path) {
        Some(group) => Ok(warp::reply::json(&group)),
        None => Err(warp::reject::not_found()),
    }
}

/// A payload sent with a request to add a group.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GroupPayload {
    pub path: String,
    pub name: String,
    pub description: Option<String>,
    pub avatar_url: Option<String>,
}

/// Handler for API route to add a group.
pub async fn post_api_group(
    user: User,
    payload: GroupPayload,
    database: Database,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to add a group: {:?}", payload);
    let re_path = Regex::new("^[a-z0-9]+((-?[a-z0-9])*[a-z0-9])?$").expect("Compile regex");
    if !re_path.is_match(&payload.path) {
        panic!(format!("Path is on invalid format: '{}'", payload.path));
    }
    if payload.path.len() > 255 {
        panic!("Path cannot be longer than 255 characters")
    }

    let new_group = NewGroup {
        path: payload.path,
        name: payload.name,
        description: payload.description,
        avatar_url: payload.avatar_url,
        added: Utc::now(),
    };
    let group = database.add_group(new_group, &user);
    Ok(warp::reply::json(&group))
}

#[derive(Debug, Serialize, Deserialize)]
struct StructuredFormatting {
    pub main_text: String,
    pub secondary_text: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct PlacePrediction {
    description: String,
    place_id: String,
    structured_formatting: StructuredFormatting,
}

#[derive(Debug, Serialize, Deserialize)]
struct PlacePredictionResult {
    pub predictions: Vec<PlacePrediction>,
}

#[derive(Debug, Serialize, Deserialize)]
struct PlaceDetails {
    pub formatted_address: String,
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct PlaceDetailsResult {
    pub result: PlaceDetails,
}

pub async fn get_api_place_predictions(
    query_params: HashMap<String, String>,
) -> Result<impl warp::Reply, warp::Rejection> {
    // TODO: Translate into rejection.
    let query = query_params
        .get("query")
        .expect("Missing query parameter 'query'");
    println!(
        "Received request to get Google Maps place predictions: '{}'",
        query
    );
    let key = std::env::var("MAPS_API_KEY").expect("Failed to get $MAPS_API_KEY");
    let url: reqwest::Url = reqwest::Url::parse_with_params(
        "https://maps.googleapis.com/maps/api/place/autocomplete/json",
        &[("key", &key), ("input", &query)],
    )
    .expect("Failed to parse URL");
    let predictions: Vec<PlacePrediction> = match reqwest::get(url)
        .await
        // TODO: Translate into rejection
        .expect("Failed to get Google Maps location predictions")
        .json::<PlacePredictionResult>()
        .await
    {
        Ok(rslt) => rslt.predictions,
        Err(_) => vec![],
    };
    Ok(warp::reply::json(&predictions))
}

/// Handler for API route to get place details/signed Google Maps static API URL corresponding
/// to a certain place ID/address description.
pub async fn get_api_place_details(
    query: HashMap<String, String>,
) -> Result<impl warp::Reply, warp::Rejection> {
    // TODO: Turn into rejection
    let place_id = query
        .get("placeId")
        .expect("Query parameter 'placeId' missing");
    println!(
        "Received request to get Google Maps image URI - placeId: {}",
        place_id
    );

    let api_key = std::env::var("MAPS_API_KEY").expect("Failed to get $MAPS_API_KEY");

    let client = reqwest::Client::new();
    let details: PlaceDetails = client
        .get("https://maps.googleapis.com/maps/api/place/details/json")
        .query(&[
            ("key", &api_key),
            ("place_id", place_id),
            ("fields", &String::from("formatted_address,name")),
        ])
        .send()
        .await
        .expect("Failed to get Google Maps place details")
        .json::<PlaceDetailsResult>()
        .await
        // TODO: Translate into rejection
        .expect("Failed to deserialize response from Google Maps")
        .result;

    let map_url = google_maps::get_map_url(&details.formatted_address, &api_key);

    let view_model = LocationDetailsViewModel {
        address: details.formatted_address,
        name: details.name,
        map_url,
    };
    Ok(warp::reply::json(&view_model))
}

/// Representation of a location for a meet.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MeetLocation {
    place_id: String,
    name: String,
    address: String,
}

/// A payload sent with a request to add a meet.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MeetPayload {
    pub group_path: String,
    pub title: String,
    pub location: MeetLocation,
    pub starts: DateTime<Utc>,
    pub ends: Option<DateTime<Utc>>,
    pub description: String,
    // TODO: Turn into references
    pub hosts: Vec<String>,
    pub attendee_limit: Option<i32>,
}

/// Handler for API route to add a meet.
pub async fn post_api_meet(
    _: User,
    payload: MeetPayload,
    database: Database,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to add a meet: {:?}", payload);
    let uuid = Uuid::new_v4().to_string();
    let group = database
        .get_group_by_path(&payload.group_path)
        .expect("Get group");
    let new_meet = NewMeet {
        uuid,
        group_id: group.id,
        title: payload.title,
        location_place_id: payload.location.place_id,
        location_name: payload.location.name,
        location_address: payload.location.address,
        starts: payload.starts,
        ends: payload
            .ends
            .unwrap_or(payload.starts.add(Duration::hours(3))),
        description: payload.description,
        hosts: payload.hosts.join(","),
        attendee_limit: payload.attendee_limit,
        added: Utc::now(),
    };
    let meet = database.add_meet(new_meet);
    Ok(warp::reply::json(&meet))
}

/// Handler for API route to get a meet.
pub async fn get_api_meet(
    uuid: String,
    database: Database,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to get meet '{}'", uuid);
    match database.get_meet_by_uuid(&uuid) {
        Some(meet) => Ok(warp::reply::json(&meet)),
        None => Err(warp::reject::not_found()),
    }
}

/// A payload sent with a request to add a meet attendee.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AttendeePayload {}

/// Handler for API route to add an attendee to a meet.
pub async fn post_api_meet_attendee(
    session: User,
    uuid: String,
    _: AttendeePayload,
    database: Database,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received request to add attendee '{}' to meet '{}'",
        session.username, uuid
    );
    database.add_meet_attendee(&uuid, &session.username);
    Ok(warp::reply())
}

/// Handler for API route to remove an attendee from a meet.
pub async fn delete_api_meet_attendee(
    session: User,
    uuid: String,
    username: String,
    database: Database,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received request to remove attendee '{}' from meet '{}'",
        username, uuid
    );
    if username != session.username {
        // TODO: Turn into rejection
        panic!("Logged in user differs from attendee");
    }
    database.remove_meet_attendee(&uuid, &username);
    Ok(warp::reply())
}

use hmac::Mac;

/// Get a Google Maps URL from an address.
pub fn get_map_url(address: &str, api_key: &str) -> String {
    let zoom = "17";
    let size = "300x187";
    let markers = format!("|{}", address);

    // Construct basic URL, with a marker for the requested location
    let url: reqwest::Url = reqwest::Url::parse_with_params(
        "https://maps.googleapis.com/maps/api/staticmap",
        &[
            ("zoom", zoom),
            ("size", size),
            ("markers", &markers),
            ("key", api_key),
        ],
    )
    .expect("Failed to parse URL");
    let url_to_sign = format!(
        "{}?{}",
        url.path(),
        url.query().expect("Failed to get query string")
    );
    println!("Signing URL '{}'", url_to_sign);
    let signing_key_b64 =
        std::env::var("MAPS_STATIC_SIGNING_KEY").expect("Failed to get $MAPS_STATIC_SIGNING_KEY");
    // The signing key will be URL safe base64 encoded, where - is replaced with + and _ with /
    let signing_key_b64 = str::replace(&signing_key_b64, "-", "+");
    let signing_key_b64 = str::replace(&signing_key_b64, "_", "/");
    let signing_key = base64::decode(&signing_key_b64).expect("Failed to decode signing key");

    // Make URL signature
    let mut mac = hmac::Hmac::<sha1::Sha1>::new_varkey(&signing_key).expect("Failed to create mac");
    mac.input(&url_to_sign.as_bytes());
    let signature = mac.result();
    // Get URL safe base64 encoded signature
    let enc_signature = base64::encode(&signature.code());
    let enc_signature = str::replace(&enc_signature, "+", "-");
    let enc_signature = str::replace(&enc_signature, "/", "_");

    let signed_url = reqwest::Url::parse_with_params(
        "https://maps.googleapis.com/maps/api/staticmap",
        &[
            ("zoom", zoom),
            ("size", size),
            ("markers", &markers),
            ("key", &api_key),
            ("signature", &enc_signature),
        ],
    )
    .expect("Failed to parse URL");
    String::from(signed_url.as_str())
}

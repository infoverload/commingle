use chrono::Utc;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenv::dotenv;
use md5::{Digest, Md5};
use std::convert::TryFrom;
use std::env;
use std::sync::{Arc, Mutex};

pub mod models;
mod schema;

use super::viewmodels::*;
use crate::google_maps;
pub use models::*;

#[derive(Clone)]
pub struct Database {
    conn: Arc<Mutex<PgConnection>>,
}

impl Database {
    pub fn new() -> Self {
        dotenv().expect("You need a .env file");

        let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        let conn = PgConnection::establish(&database_url)
            .expect(&format!("Error connecting to {}", database_url));
        Self {
            conn: Arc::new(Mutex::new(conn)),
        }
    }

    /// Get user by ID.
    pub fn get_user(&self, id: i64) -> Option<User> {
        let conn = self.conn.lock().expect("Lock self.conn");
        use schema::users;
        let results = users::table
            .filter(users::id.eq(i32::try_from(id).expect("Conversion from i64 to i32")))
            .limit(1)
            .load::<User>(&*conn)
            .expect("Error searching for user");
        if results.len() == 1 {
            let user = results[0].clone();
            println!("Found user {}", user.username);
            Some(user)
        } else {
            println!("Couldn't find user");
            None
        }
    }

    /// Get user by username.
    pub fn get_user_by_username(&self, username: &str) -> Option<User> {
        let conn = self.conn.lock().expect("Lock self.conn");
        use schema::users;
        let results = users::table
            .filter(users::username.eq(username))
            .limit(1)
            .load::<User>(&*conn)
            .expect("Error searching for user");
        if results.len() == 1 {
            let user = results[0].clone();
            println!("Found user {}", user.username);
            Some(user)
        } else {
            println!("Couldn't find user {}", username);
            None
        }
    }

    /// Get user by username or email address.
    pub fn get_user_by_username_or_email(&self, username_or_email: &str) -> Option<User> {
        let conn = self.conn.lock().expect("Lock self.conn");
        use schema::users;
        let results = users::table
            // TODO: Search also by email
            .filter(users::username.eq(username_or_email))
            .limit(1)
            .load::<User>(&*conn)
            .expect("Error searching for user");
        if results.len() == 1 {
            let user = results[0].clone();
            println!("Found user {}", user.username);
            Some(user)
        } else {
            println!("Couldn't find user by username/email {}", username_or_email);
            None
        }
    }

    pub fn add_user(&self, user: NewUser) -> User {
        let conn = self.conn.lock().expect("Lock self.conn");
        use schema::users;

        let user: User = diesel::insert_into(users::table)
            .values(&user)
            .get_result(&*conn)
            .expect("Add user");
        println!("Inserted user with ID {}\n", user.id);
        user
    }

    /// Get a user's groups.
    pub fn get_user_groups(&self, username: &str) -> Vec<GroupViewModel> {
        use schema::{groups, users, users_groups};
        let conn = self.conn.lock().expect("Lock self.conn");
        let groups = users::table
            .inner_join(users_groups::table.inner_join(groups::table))
            .filter(users::username.eq(username))
            .select(groups::all_columns)
            .load::<Group>(&*conn)
            .expect("Error loading user's groups");
        groups
            .iter()
            .map(|group| GroupViewModel {
                path: group.path.clone(),
                name: group.name.clone(),
                description: group.description.clone(),
                added: group.added,
                avatar_url: group.avatar_url.clone(),
                members: vec![],
                upcoming: vec![],
                past: vec![],
            })
            .collect()
    }

    /// Get a user's upcoming meets.
    pub fn get_user_upcoming_meets(&self, username: &str) -> Vec<MeetViewModel> {
        use schema::{meets, meets_users, users};
        let conn = self.conn.lock().expect("Lock self.conn");
        let now = Utc::now();
        let meets: Vec<Meet> = meets::table
            .inner_join(meets_users::table.inner_join(users::table))
            .filter(users::username.eq(username))
            .filter(meets::ends.ge(now))
            .select(meets::all_columns)
            .load::<Meet>(&*conn)
            .expect("Error loading user's meets")
            .into_iter()
            .collect();

        meets
            .iter()
            .map(|meet| MeetViewModel::new(&meet, "", "", vec![]))
            .collect()
    }

    /// Get a user's past meets.
    pub fn get_user_past_meets(&self, username: &str) -> Vec<MeetViewModel> {
        use schema::{meets, meets_users, users};
        let conn = self.conn.lock().expect("Lock self.conn");
        let now = Utc::now();
        let meets: Vec<Meet> = meets::table
            .inner_join(meets_users::table.inner_join(users::table))
            .filter(users::username.eq(username))
            .filter(meets::ends.lt(now))
            .select(meets::all_columns)
            .load::<Meet>(&*conn)
            .expect("Error loading user's meets")
            .into_iter()
            .collect();

        meets
            .iter()
            .map(|meet| MeetViewModel::new(&meet, "", "", vec![]))
            .collect()
    }

    /// Get group view model by path.
    pub fn get_group_view_model_by_path(&self, path: &str) -> Option<GroupViewModel> {
        let conn = self.conn.lock().expect("Lock self.conn");
        use schema::{groups, users, users_groups};
        let results = groups::table
            .filter(groups::path.eq(path))
            .limit(1)
            .load::<Group>(&*conn)
            .expect("Error searching for group");
        if results.len() == 0 {
            println!("Couldn't find group '{}'", path);
            return None;
        }

        let group = results[0].clone();
        println!("Found group '{}'", group.path);

        let members = groups::table
            .inner_join(users_groups::table.inner_join(users::table))
            .filter(groups::path.eq(path))
            .select(users::all_columns)
            .load::<User>(&*conn)
            .expect("Error loading group's members");
        let meets = Meet::belonging_to(&group)
            .load::<Meet>(&*conn)
            .expect("Error loading group's meets");
        let now = Utc::now();
        let view_model = GroupViewModel {
            path: group.path,
            name: group.name,
            added: group.added,
            avatar_url: group.avatar_url,
            description: group.description,
            members: members
                .iter()
                .map(|member| {
                    let avatar_url = member.avatar_url.clone().unwrap_or_else(|| {
                        let mut hasher = Md5::new();
                        hasher.input(member.email_address.to_lowercase());
                        let email_hash = hasher.result();
                        format!(
                            "http://www.gravatar.com/avatar/{:x}?d=identicon&s=300",
                            email_hash
                        )
                    });

                    GroupMemberViewModel {
                        username: member.username.clone(),
                        name: member.name.clone(),
                        avatar_url,
                        bio: member.bio.clone(),
                    }
                })
                .collect(),
            upcoming: meets
                .iter()
                .filter(|meet| meet.ends >= now)
                .map(|meet| {
                    GroupMeetViewModel {
                        uuid: meet.uuid.clone(),
                        title: meet.title.clone(),
                        location_place_id: meet.location_place_id.clone(),
                        location_name: meet.location_name.clone(),
                        location_address: meet.location_address.clone(),
                        starts: meet.starts.clone(),
                        ends: meet.ends.clone(),
                        attendee_limit: meet.attendee_limit.clone(),
                        // TODO
                        going: vec![],
                    }
                })
                .collect(),
            past: meets
                .iter()
                .filter(|meet| meet.ends < now)
                .map(|meet| {
                    GroupMeetViewModel {
                        uuid: meet.uuid.clone(),
                        title: meet.title.clone(),
                        location_place_id: meet.location_place_id.clone(),
                        location_name: meet.location_name.clone(),
                        location_address: meet.location_address.clone(),
                        starts: meet.starts.clone(),
                        ends: meet.ends.clone(),
                        attendee_limit: meet.attendee_limit.clone(),
                        // TODO
                        going: vec![],
                    }
                })
                .collect(),
        };
        Some(view_model)
    }

    /// Get group by path.
    pub fn get_group_by_path(&self, path: &str) -> Option<Group> {
        let conn = self.conn.lock().expect("Lock self.conn");
        use schema::groups;
        let results = groups::table
            .filter(groups::path.eq(path))
            .limit(1)
            .load::<Group>(&*conn)
            .expect("Error searching for group");
        if results.len() == 0 {
            println!("Couldn't find group '{}'", path);
            return None;
        }

        let group = results[0].clone();
        println!("Found group '{}'", group.path);
        Some(group)
    }

    /// Add a group.
    pub fn add_group(&self, group: NewGroup, user: &User) -> Group {
        let conn = self.conn.lock().expect("Lock self.conn");
        use schema::{groups, users_groups};

        // TODO: Enclose in transaction
        let group: Group = diesel::insert_into(groups::table)
            .values(&group)
            .get_result(&*conn)
            .expect("Add group");
        println!("Inserted group with ID {}, path {}\n", group.id, group.path);
        // TODO: Flag user as group owner
        let user_group = NewUserGroup {
            user_id: user.id,
            group_id: group.id,
            added: Utc::now(),
        };
        diesel::insert_into(users_groups::table)
            .values(&user_group)
            .execute(&*conn)
            .expect("Add user as group member");
        group
    }

    pub fn add_meet(&self, meet: NewMeet) -> Meet {
        let conn = self.conn.lock().expect("Lock self.conn");
        use schema::meets;

        let meet: Meet = diesel::insert_into(meets::table)
            .values(&meet)
            .get_result(&*conn)
            .expect("Add meet");
        println!("Inserted meet with ID {}, UUID {}\n", meet.id, meet.uuid);
        meet
    }

    pub fn get_meet_by_uuid(&self, uuid: &str) -> Option<MeetViewModel> {
        let conn = self.conn.lock().expect("Lock self.conn");
        use schema::{groups, meets, meets_users, users};
        let results = meets::table
            .filter(meets::uuid.eq(uuid))
            .limit(1)
            .load::<Meet>(&*conn)
            .expect("Error searching for meet");
        if results.len() == 0 {
            println!("Couldn't find meet '{}'", uuid);
            return None;
        }

        let meet = results[0].clone();
        println!("Found meet '{}'", meet.uuid);

        let results = groups::table
            .filter(groups::id.eq(&meet.group_id))
            .limit(1)
            .load::<Group>(&*conn)
            .expect("Error searching for group");
        if results.len() == 0 {
            panic!(format!("Couldn't find group {}", meet.group_id));
        }
        let group = results[0].clone();

        let api_key = std::env::var("MAPS_API_KEY").expect("Failed to get $MAPS_API_KEY");
        let location_map_url = google_maps::get_map_url(&meet.location_address, &api_key);

        // Get usernames of attendees
        let attendees: Vec<String> = meets::table
            .inner_join(meets_users::table.inner_join(users::table))
            .filter(meets::id.eq(&meet.id))
            .select(users::all_columns)
            .load::<User>(&*conn)
            .expect("Error loading meet's attendees")
            .into_iter()
            .map(|user| user.username)
            .collect();

        let view_model = MeetViewModel::new(&meet, &location_map_url, &group.path, attendees);
        Some(view_model)
    }

    pub fn add_meet_attendee(&self, uuid: &str, username: &str) {
        use schema::{meets, meets_users, users};
        let conn = self.conn.lock().expect("Lock self.conn");
        let results = meets::table
            .filter(meets::uuid.eq(uuid))
            .limit(1)
            .load::<Meet>(&*conn)
            .expect("Error searching for meet");
        if results.len() == 0 {
            panic!(format!("Couldn't find meet '{}'", uuid));
        }
        let meet = results[0].clone();

        let results = users::table
            .filter(users::username.eq(username))
            .limit(1)
            .load::<User>(&*conn)
            .expect("Error searching for user");
        if results.len() == 0 {
            panic!(format!("Couldn't find user '{}'", username));
        }
        let user = results[0].clone();

        let attendance = NewAttendance {
            meet_id: meet.id,
            user_id: user.id,
        };
        diesel::insert_into(meets_users::table)
            .values(&attendance)
            .execute(&*conn)
            .expect("Add meet attendance");
    }

    pub fn remove_meet_attendee(&self, uuid: &str, username: &str) {
        use schema::{meets, meets_users, users};
        let conn = self.conn.lock().expect("Lock self.conn");
        let results = meets::table
            .filter(meets::uuid.eq(uuid))
            .limit(1)
            .load::<Meet>(&*conn)
            .expect("Error searching for meet");
        if results.len() == 0 {
            panic!(format!("Couldn't find meet '{}'", uuid));
        }
        let meet = results[0].clone();

        let results = users::table
            .filter(users::username.eq(username))
            .limit(1)
            .load::<User>(&*conn)
            .expect("Error searching for user");
        if results.len() == 0 {
            panic!(format!("Couldn't find user '{}'", username));
        }
        let user = results[0].clone();

        diesel::delete(
            meets_users::table.filter(
                meets_users::meet_id
                    .eq(meet.id)
                    .and(meets_users::user_id.eq(user.id)),
            ),
        )
        .execute(&*conn)
        .expect("Failed to de-register meet attendance");
    }
}

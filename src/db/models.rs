use super::schema::*;
use chrono::{DateTime, Utc};
use diesel::{Identifiable, Queryable};
use serde::{Deserialize, Serialize};

/// Representation of a user to be added to the database.
#[derive(Serialize, Deserialize, Insertable)]
#[table_name = "users"]
#[serde(rename_all = "camelCase")]
pub struct NewUser {
    pub username: String,
    pub name: String,
    pub email_address: String,
    pub password_hash: String,
    pub added: DateTime<Utc>,
    pub avatar_url: Option<String>,
    pub bio: Option<String>,
}

impl NewUser {
    pub fn new(
        username: String,
        name: String,
        email_address: String,
        password_hash: String,
        bio: Option<String>,
    ) -> Self {
        Self {
            username,
            name,
            email_address,
            password_hash,
            added: Utc::now(),
            avatar_url: None,
            bio,
        }
    }
}

#[derive(Identifiable, Debug, Serialize, Deserialize, Clone, Queryable)]
#[serde(rename_all = "camelCase")]
#[table_name = "users"]
pub struct User {
    pub id: i32,
    pub username: String,
    pub name: String,
    pub email_address: String,
    #[serde(skip_serializing)]
    pub password_hash: String,
    pub added: DateTime<Utc>,
    pub avatar_url: Option<String>,
    pub bio: Option<String>,
}

/// Representation of a group to be added to the database.
#[derive(Serialize, Deserialize, Insertable)]
#[table_name = "groups"]
#[serde(rename_all = "camelCase")]
pub struct NewGroup {
    pub path: String,
    pub name: String,
    pub added: DateTime<Utc>,
    pub avatar_url: Option<String>,
    pub description: Option<String>,
}

#[derive(Serialize, Identifiable, Debug, Clone, Queryable)]
#[table_name = "groups"]
#[serde(rename_all = "camelCase")]
pub struct Group {
    #[serde(skip_serializing)]
    pub id: i32,
    pub path: String,
    pub name: String,
    pub added: DateTime<Utc>,
    pub avatar_url: Option<String>,
    pub description: String,
}

/// Representation of a user<->group membership to be added to the database.
#[derive(Debug, Serialize, Deserialize, Insertable)]
#[table_name = "users_groups"]
pub struct NewUserGroup {
    pub user_id: i32,
    pub group_id: i32,
    pub added: DateTime<Utc>,
}

/// A many-to-many association between users and groups.
#[derive(Identifiable, Associations, Debug, Serialize, Deserialize, Clone, Queryable)]
#[table_name = "users_groups"]
#[belongs_to(User)]
#[belongs_to(Group)]
pub struct UserGroup {
    pub id: i32,
    pub user_id: i32,
    pub group_id: i32,
    pub added: DateTime<Utc>,
}

/// Representation of a meet to be added to the database.
#[derive(Debug, Serialize, Deserialize, Insertable)]
#[serde(rename_all = "camelCase")]
#[table_name = "meets"]
pub struct NewMeet {
    pub uuid: String,
    pub title: String,
    pub location_place_id: String,
    pub location_name: String,
    pub location_address: String,
    pub starts: DateTime<Utc>,
    pub ends: DateTime<Utc>,
    pub description: String,
    pub hosts: String,
    pub attendee_limit: Option<i32>,
    pub added: DateTime<Utc>,
    #[serde(skip_serializing)]
    pub group_id: i32,
}

#[derive(Identifiable, Debug, Serialize, Deserialize, Clone, Queryable, Associations)]
#[serde(rename_all = "camelCase")]
#[belongs_to(Group)]
#[table_name = "meets"]
pub struct Meet {
    #[serde(skip_serializing)]
    pub id: i32,
    pub uuid: String,
    pub title: String,
    /// ID of a Google Maps location.
    pub location_place_id: String,
    pub starts: DateTime<Utc>,
    pub ends: DateTime<Utc>,
    pub description: String,
    // TODO: Turn into references
    pub hosts: String,
    pub attendee_limit: Option<i32>,
    pub added: DateTime<Utc>,
    #[serde(skip_serializing)]
    pub group_id: i32,
    /// Name of a Google Maps location.
    pub location_name: String,
    /// Address of a Google Maps location.
    pub location_address: String,
}

/// Representation of a meet attendance to be added to the database.
#[derive(Debug, Serialize, Deserialize, Insertable)]
#[serde(rename_all = "camelCase")]
#[table_name = "meets_users"]
pub struct NewAttendance {
    pub meet_id: i32,
    pub user_id: i32,
}

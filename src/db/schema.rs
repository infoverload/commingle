table! {
    groups (id) {
        id -> Int4,
        path -> Varchar,
        name -> Varchar,
        added -> Timestamptz,
        avatar_url -> Nullable<Varchar>,
        description -> Varchar,
    }
}

table! {
    meets (id) {
        id -> Int4,
        uuid -> Varchar,
        title -> Varchar,
        location_place_id -> Varchar,
        starts -> Timestamptz,
        ends -> Timestamptz,
        description -> Varchar,
        hosts -> Varchar,
        attendee_limit -> Nullable<Int4>,
        added -> Timestamptz,
        group_id -> Int4,
        location_name -> Varchar,
        location_address -> Varchar,
    }
}

table! {
    meets_users (id) {
        id -> Int4,
        meet_id -> Int4,
        user_id -> Int4,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        name -> Varchar,
        email_address -> Varchar,
        password_hash -> Varchar,
        added -> Timestamptz,
        avatar_url -> Nullable<Varchar>,
        bio -> Nullable<Varchar>,
    }
}

table! {
    users_groups (id) {
        id -> Int4,
        user_id -> Int4,
        group_id -> Int4,
        added -> Timestamptz,
    }
}

joinable!(meets -> groups (group_id));
joinable!(meets_users -> meets (meet_id));
joinable!(meets_users -> users (user_id));
joinable!(users_groups -> groups (group_id));
joinable!(users_groups -> users (user_id));

allow_tables_to_appear_in_same_query!(groups, meets, meets_users, users, users_groups,);

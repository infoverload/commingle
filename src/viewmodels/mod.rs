use super::db::models::*;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GroupMemberViewModel {
    pub username: String,
    pub name: String,
    pub avatar_url: String,
    pub bio: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GroupViewModel {
    pub path: String,
    pub name: String,
    pub added: DateTime<Utc>,
    pub avatar_url: Option<String>,
    pub description: String,
    pub members: Vec<GroupMemberViewModel>,
    pub upcoming: Vec<GroupMeetViewModel>,
    pub past: Vec<GroupMeetViewModel>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GroupMeetViewModel {
    pub uuid: String,
    pub title: String,
    pub location_place_id: String,
    pub location_name: String,
    pub location_address: String,
    pub starts: DateTime<Utc>,
    pub ends: DateTime<Utc>,
    pub attendee_limit: Option<i32>,
    pub going: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MeetViewModel {
    pub uuid: String,
    pub title: String,
    pub starts: DateTime<Utc>,
    pub ends: DateTime<Utc>,
    pub description: String,
    // TODO: Turn into references
    pub hosts: String,
    pub attendee_limit: Option<i32>,
    pub added: DateTime<Utc>,
    pub group_path: String,
    pub location_name: String,
    pub location_address: String,
    pub location_map_url: String,
    pub location_place_id: String,
    pub attendees: Vec<String>,
}

impl MeetViewModel {
    pub fn new(
        meet: &Meet,
        location_map_url: &str,
        group_path: &str,
        attendees: Vec<String>,
    ) -> Self {
        Self {
            uuid: meet.uuid.clone(),
            title: meet.title.clone(),
            starts: meet.starts.clone(),
            ends: meet.ends.clone(),
            description: meet.description.clone(),
            hosts: meet.hosts.clone(),
            attendee_limit: meet.attendee_limit.clone(),
            added: meet.added.clone(),
            location_name: meet.location_name.clone(),
            location_address: meet.location_address.clone(),
            location_place_id: meet.location_place_id.clone(),
            group_path: String::from(group_path),
            location_map_url: String::from(location_map_url),
            attendees,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LocationDetailsViewModel {
    pub name: String,
    pub address: String,
    pub map_url: String,
}

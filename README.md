# Commingle
[![pipeline status](https://gitlab.com/commingle/commingle/badges/master/pipeline.svg)](https://gitlab.com/commingle/commingle/commits/master)

An open and community focused meetup platform.

## Contribute
If you wish to contribute, please join our [Discord chat server](https://discord.gg/KbJnJM).


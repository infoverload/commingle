const t = require('tcomb')
const last = require('ramda/src/last')
const pick = require('ramda/src/pick')
const mergeRight = require('ramda/src/mergeRight')
const reduce = require('ramda/src/reduce')
const join = require('ramda/src/join')

const logger = require('./logger').child({module: 'chooComp',})

const emitForComponent = (componentPath, emit) => {
  t.Array(componentPath, ['componentPath',])
  t.Function(emit, ['emit',])

  if (emit.global == null) {
    emit.global = (event, ...args) => {
      logger.debug(`Emitting global event ${event}`, args)
      emit(event, ...args)
    }
  }
  const componentEmit = (eventName, ...args) => {
    t.String(eventName, ['eventName',])
    const qualifiedEventName = `${join('.', componentPath)}.${eventName}`
    if (emit.global != null) {
      t.Function(emit.global, ['emit', 'global',])
      emit.global(qualifiedEventName, ...args)
    } else {
      emit(qualifiedEventName, ...args)
    }
  }
  componentEmit.triggerRender = () => {
    logger.debug(`Triggering render pass`)
    if (emit.global != null) {
      emit.global('render')
    } else {
      emit('render')
    }
  }
  componentEmit.global = emit.global
  return componentEmit
}

const emitterForComponent = (path, emitter, state) => {
  t.Array(path, ['path',])
  t.Object(emitter, ['emitter',])
  t.Object(state, ['state',])
  const iface = {
    on: (eventName, callback) => {
      t.String(eventName, ['eventName',])
      t.Function(callback, ['callback',])
      const qualifiedEventName = `${join('.', path)}.${eventName}`
      logger.debug(`Setting event handler for ${qualifiedEventName}:`, emitter)
      if (emitter.onGlobal != null) {
        emitter.onGlobal(qualifiedEventName, callback)
      } else {
        emitter.on(qualifiedEventName, callback)
      }
    },
    emit: (eventName, ...args) => {
      t.String(eventName, ['eventName',])
      const qualifiedEventName = `${join('.', path)}.${eventName}`
      logger.debug(`Emitting event ${qualifiedEventName}`)
      if (emitter.emitGlobal != null) {
        emitter.emitGlobal(qualifiedEventName, ...args)
      } else {
        emitter.emit(qualifiedEventName, ...args)
      }
    },
  }
  if (emitter.onGlobal != null) {
    return mergeRight(iface, {
      onGlobal: emitter.onGlobal.bind(emitter),
      emitGlobal: emitter.emitGlobal.bind(emitter),
      triggerRender: emitter.triggerRender.bind(emitter),
    })
  } else {
    return mergeRight(iface, {
      onGlobal: (eventName, callback) => {
        t.String(eventName, ['eventName',])
        t.Function(callback, ['callback',])
        logger.debug(`Setting global event handler for ${eventName}:`, emitter)
        if (emitter.onGlobal != null) {
          emitter.onGlobal(eventName, callback)
        } else {
          emitter.on(eventName, callback)
        }
      },
      emitGlobal: (eventName, ...args) => {
        t.String(eventName, ['eventName',])
        logger.debug(`Emitting global event ${eventName}`)
        if (emitter.emitGlobal != null) {
          emitter.emitGlobal(eventName, ...args)
        } else {
          emitter.emit(eventName, ...args)
        }
      },
      triggerRender: () => {
        logger.debug(`Triggering new render pass`)
        emitter.emit(state.events.RENDER)
      },
    })
  }
}

const getComponentState = (state, componentPath) => {
  t.Object(state, ['state',])
  t.Array(componentPath, ['componentPath',])
  const componentName = last(componentPath)
  const globalState = state.global == null ? state : state.global
  if (state.components == null) {
    state.components = {}
  }
  const chooProps = ['events', 'href', 'query', 'params',]
  if (state.components[componentName] != null) {
    return reduce((componentState, prop) => {
      componentState[prop] = state[prop]
      return componentState
    }, state.components[componentName], chooProps)
  } else {
    const componentState = mergeRight(pick(chooProps, state), {
      componentName: join('.', componentPath),
      componentPath,
      components: {},
      global: globalState,
    })
    state.components[componentName] = componentState
    return componentState
  }
}

const component = (properties) => {
  return (componentPath) => {
    t.Array(componentPath, ['componentPath',])
    const componentName = last(componentPath)
    const iface = {
      // When initializing a component, it gets added to the parent component's component
      // state tree
      initialize: (state, emitter, ...args) => {
        t.Object(state, ['state',])
        t.Object(emitter, ['emitter',])

        const componentState = getComponentState(state, componentPath)
        if (properties.initialize != null) {
          const qualifiedComponentName = join('.', componentPath)
          logger.debug(`Calling initialize hook of component ${qualifiedComponentName}`)
          try {
            properties.initialize(componentState, emitterForComponent(
              componentPath, emitter, state), ...args)
          } catch (error) {
            logger.error(`Initialization hook of component ${qualifiedComponentName} failed:`,
              error)
          }
          logger.debug(`Initialization hook of component ${qualifiedComponentName} finished`)
        }
      },
      loadData: async (state, emit) => {
        t.Object(state, ['state',])
        t.Object(state.events, ['state', 'events',])
        t.String(state.href, ['state', 'href',])
        t.Object(state.params, ['state', 'params',])
        t.Object(state.query, ['state', 'query',])
        t.Function(emit, ['emit',])
        if (properties.loadData != null) {
          const qualifiedComponentName = join('.', componentPath)
          const componentState = getComponentState(state, componentPath)
          t.Object(componentState, ['state', 'components', componentName,])
          logger.debug(`Calling loadData hook of component ${qualifiedComponentName}`)
          try {
            await properties.loadData(componentState, emitForComponent(componentPath, emit))
          } catch (err) {
            logger.error(`loadData hook of component ${qualifiedComponentName} failed:`, err)
          }

          logger.debug(`loadData hook of component ${qualifiedComponentName} finished`)
        }
      },
      resetState: (state, opts) => {
        t.Object(state, ['state',])
        if (opts != null) {
          t.Object(opts, ['opts',])
        }

        if (properties.resetState != null) {
          const qualifiedComponentName = join('.', componentPath)
          const componentState = getComponentState(state, componentPath)
          t.Object(componentState, ['state', 'components', componentName,])
          logger.debug(`Calling resetState hook of component ${qualifiedComponentName}`)
          try {
            properties.resetState(componentState, opts)
          } catch (error) {
            logger.error(`resetState hook of component ${qualifiedComponentName} failed:`,
              error)
          }
          logger.debug(`resetState hook of component ${qualifiedComponentName} finished`)
        }
      },
      render: (state, emit, ...args) => {
        t.Object(state, ['state',])
        t.Object(state.events, ['state', 'events',])
        t.String(state.href, ['state', 'href',])
        t.Object(state.params, ['state', 'params',])
        t.Object(state.query, ['state', 'query',])
        t.Function(emit, ['emit',])
        const qualifiedComponentName = join('.', componentPath)
        logger.debug(`Rendering component ${qualifiedComponentName}...`)
        const componentState = getComponentState(state, componentPath)
        t.Object(componentState, ['state', 'components', componentName,])
        if (componentState.isLoading) {
          logger.debug(`Component ${qualifiedComponentName} isn't ready`)
          return []
        } else {
          logger.debug(`Calling render hook of component ${qualifiedComponentName}, state:`,
            componentState)
          return properties.render(componentState, emitForComponent(componentPath, emit), ...args)
        }
      },
    }
    if (properties.onLoad != null) {
      t.Function(properties.onLoad, ['properties', 'onLoad',])
      iface.onLoad = (state, emit, ...args) => {
        t.Object(state, ['state',])
        t.Function(emit, ['emit',])
        const componentState = getComponentState(state, componentPath)
        t.Object(componentState, ['state', 'components', componentName,])
        properties.onLoad(componentState, emitForComponent(componentPath, emit), ...args)
      }
    }
    if (properties.onUnload != null) {
      t.Function(properties.onUnload, ['properties', 'onUnload',])
      iface.onUnload = (state) => {
        t.Object(state, ['state',])
        const componentState = getComponentState(state, componentPath)
        properties.onUnload(componentState)
      }
    }
    return iface
  }
}

const getQualifiedElementId = (elementId, state) => {
  t.String(elementId, ['elementId',])
  t.Object(state, ['state',])
  return`${elementId}-${state.componentName.replace('.', '-')}`
}

module.exports = {
  emitterForComponent,
  emitForComponent,
  component,
  getQualifiedElementId,
}

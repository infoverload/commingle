const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const assert = require('assert')
const MarkdownSanitizer = require('@arve.knudsen/pagedown/Markdown.Sanitizer')
const {DateTime,} = require('luxon')
const map = require('ramda/src/map')
const flatten = require('ramda/src/flatten')
const pipe = require('ramda/src/pipe')
const prepend = require('ramda/src/prepend')
const includes = require('ramda/src/includes')
const equals = require('ramda/src/equals')
const reject = require('ramda/src/reject')

const loadingComponent = require('../components/loading')(['views', 'meet', 'loading',])
const createIconElem = require('../createIconElem')
const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/meet',})

const sheet = sheetify('./meet.styl')

const domParser = new DOMParser()

const renderMarkdown = (markdown) => {
  t.String(markdown, ['markdown',])
  const converter = MarkdownSanitizer.getSanitizingConverter()
  const html = converter.makeHtml(markdown)
  const dom = domParser.parseFromString(`<div>${html}</div>`, 'text/html')
  if (dom == null) {
    logger.error(`Failed to parse HTML:`, html)
    throw new Error(`Failed to parse HTML`)
  }

  return dom.body.children[0]
}

const renderAttendanceControls = (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  t.Function(emit, ['emit',])
  const {meet,} = state
  t.Array(meet.attendees, ['meet', 'attendees',])
  const {loggedInUser,} = state.global
  if (loggedInUser == null) {
    logger.debug(`User not logged in - not rendering attendance controls`)
    return null
  }
  if (state.isRegisteringAttendance) {
    logger.debug(`User attendance being registered, rendering loader`)
    return h(`#attendance-controls`, [
      loadingComponent.render(state, emit),
    ])
  }

  const isAttending = includes(loggedInUser.username, meet.attendees)
  logger.debug(`User logged in - rendering attendance controls`, {
    isAttending,
  })

  return h(`#attendance-controls`, [
    h(`.button-group`, [
      h(`button#attend-button`, {
        onclick: () => {
          if (isAttending) {
            logger.debug(`User clicked button to no longer attend meet`)
            state.isRegisteringAttendance = true
            emit.triggerRender()
            ajax.delete(`/api/meets/${meet.uuid}/attendees/${loggedInUser.username}`)
              .then(() => {
                meet.attendees = reject(equals(loggedInUser.username), meet.attendees)
                logger.debug(`Succesfully registered user as not attending meet`)
              })
              .finally(() => {
                state.isRegisteringAttendance = false
                emit.triggerRender()
              })
          } else {
            logger.debug(`User clicked button to attend meet`)
            state.isRegisteringAttendance = true
            emit.triggerRender()
            ajax.postJson(`/api/meets/${meet.uuid}/attendees`, {})
              .then(() => {
                meet.attendees.push(loggedInUser.username)
                logger.debug(`Succesfully registered user as attending meet`)
              })
              .finally(() => {
                state.isRegisteringAttendance = false
                emit.triggerRender()
              })
          }
        },
      }, [
        isAttending ? `Attending` : `Attend`,
      ]),
    ]),
  ])
}

const renderRightColumn = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const {meet,} = state
  const {loggedInUser,} = state
  const startTime = DateTime.fromISO(meet.starts).toLocal()
  const endTime = DateTime.fromISO(meet.ends).toLocal()
  const endDateAndTimeStr = !endTime.hasSame(startTime, 'day') ? endTime.toLocaleString({
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: '2-digit',
  }) : endTime.toLocaleString({
    hour: 'numeric',
    minute: '2-digit',
  })
  const dateAndTime = `${startTime.toLocaleString({
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: '2-digit',
  })} - ${endDateAndTimeStr}`
  let hosts = meet.hosts
  // TODO: Hosts should be an array of objects
  if (typeof hosts === 'string') {
    hosts = [hosts,]
  }
  const hostElems = pipe(
    map((host) => {
      const link = h(`a`, {href: `/u/${host}`,}, host)
      if (host === hosts[0]) {
        return link
      }
      if (host === hosts[hosts.length - 1]) {
        return [` and `, host,]
      }
      return [`, `, host,]
    }),
    flatten,
    prepend(`Organized by `)
  )(hosts)
  const mapUrl = encodeURI(`https://google.com/maps/search/?api=1&query=${
      meet.locationAddress}&query_place_id=${meet.locationPlaceId}`)
  let attendanceStr = `Noone has signed up yet`
  let attendees = loggedInUser == null ? meet.attendees : reject(equals(loggedInUser.username),
    meet.attendees)
  if (attendees.length > 0) {
    attendanceStr = attendees.length == 1 ? `1 person is attending` :
      `${attendees.length} people are attending`
  }
  return h(`#right-column`, [
    h(`#meet-pad`, [
      h(`.meet-meta`, [
        h(`#meet-title-container`, [
          h(`#meet-title`, meet.title),
          h(`#meet-organizers`, hostElems),
          renderAttendanceControls(state, emit),
        ]),
        h('hr'),
        h(`#meet-time`, [
          createIconElem('clock', {classes: ['meta-icon',],}),
          dateAndTime,
        ]),
        h('hr'),
        h(`#meet-location`, [
          h(`#meet-location-inner`, {
            onclick: () => {
              logger.debug(`Location clicked, toggling map`)
              state.shouldShowMap = !state.shouldShowMap
              emit.triggerRender()
            },
          }, [
            createIconElem('location', {classes: ['meta-icon',],}),
            h(`#location-name-and-address`, [
              h(`#location-name`, meet.locationName),
              h(`#location-address`, meet.locationAddress),
            ]),
          ]),
          h(`#location-map-wrapper${state.shouldShowMap ? '' : '.hidden'}`, h(`a`, {
            href: mapUrl,
            target: `_blank`,
            rel: `noreferrer noopener`,
          }, h(`img`, {
            src: meet.locationMapUrl,
          }))),
        ]),
        h('hr'),
        h(`#attendees`, [
          createIconElem('users', {classes: ['meta-icon',],}),
          attendanceStr,
        ]),
        h('hr'),
      ]),
      h(`#meet-description-container`, [
        h(`h3`, `Details`),
        h(`#meet-description`, renderMarkdown(meet.description)),
      ]),
    ]),
  ])
}

module.exports = {
  name: 'meetView',
  route: '/m/:uuid',
  loadData: async (state, emit) => {
    t.Object(state, ['state',])
    t.Function(emit, ['emit',])
    const {uuid,} = state.params
    assert.ok(!isNullOrBlank(uuid))
    logger.debug(`Loading meet ${uuid}...`)
    const meet = await ajax.getJson(`/api/meets/${uuid}`)
    logger.debug(`Successfully loaded meet '${uuid}'`)
    state.meet = meet
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])
    const {meet,} = state
    t.Object(meet, ['state', 'meet',])

    logger.debug(`Rendering meet ${meet.uuid}`, {meet,})
    return h(`.${sheet}`, h('#meet-view', [
      renderRightColumn(state, emit),
    ]))
  },
}

const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const assert = require('assert')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const {DateTime,} = require('luxon')
const map = require('ramda/src/map')
const addIndex = require('ramda/src/addIndex')
const titleize = require('titleize')
const jdenticon = require('jdenticon')
const isEmpty = require('ramda/src/isEmpty')
const pipe = require('ramda/src/pipe')
const groupBy = require('ramda/src/groupBy')
const toPairs = require('ramda/src/toPairs')

const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/groupProfile',})

const sheet = sheetify('./groupProfile.styl')

// Render upcoming meets.
const renderUpcoming = (state) => {
  t.Object(state, ['state',])
  const {group,} = state
  t.Object(group, ['state.group',])

  if (isEmpty(group.upcoming || [])) {
    return h(`#group-upcoming.empty-group-tab`, `${group.name} doesn't have any upcoming meets.`)
  }

  return h(`#group-upcoming`, h(`ul`, pipe(
    groupBy((meet) => {
      logger.debug(`Grouping by ${DateTime.fromISO(meet.starts).toLocal().toISODate()}`)
      return DateTime.fromISO(meet.starts).toLocal().toISODate()
    }),
    toPairs,
    map(([dateStr, meets,]) => {
      const date = DateTime.fromISO(dateStr).toLocal()
      return h(`li.date-with-meets`, [
        h(`.meet-date`, date.toLocaleString(DateTime.DATE_FULL)),
        h(`ul.meets-for-date`, map((meet) => {
          const starts = DateTime.fromISO(meet.starts).toLocal()
          const ends = DateTime.fromISO(meet.ends).toLocal()
          const endStr = ends.diff(starts).days === 0 ? ends.toLocaleString(DateTime.TIME_SIMPLE) :
            ends.toLocaleString(DateTime.DATETIME_SHORT)
          const timeStr = `${starts.toLocaleString(DateTime.DATETIME_SHORT)} - ${endStr}`
          return h(`li.meet-container`, [
            h(`.meet-content`, [
              h(`a`, {href: `/m/${meet.uuid}`,}, [
                h(`.meet-title`, `${meet.title}`),
                h(`.meet-time`, timeStr),
                h(`.meet-location`, meet.location),
                h(`.meet-going`, isEmpty(meet.going) ? `Noone has signed up yet` :
                  `${meet.going.length} going`),
              ]),
            ]),
          ])
        }, meets)),
      ])
    })
  )(group.upcoming)))
}

// Render past meets.
const renderPast = (state) => {
  t.Object(state, ['state',])
  const {group,} = state
  t.Object(group, ['state.group',])

  if (isEmpty(group.past || [])) {
    return h(`#group-past.empty-group-tab`, `${group.name} doesn't have any past meets.`)
  }

  return h(`#group-past`, map((meet) => {
    return h(`.meet-container`, [
      h(`.meet-content`, [
        h(`a`, {href: `/m/${meet.uuid}`,}, [
          h(`.meet-title`, `${meet.title})`),
          h(`.meet-location`, meet.location),
          h(`.meet-went`, meet.going.length),
        ]),
      ]),
    ])
  }, group.past))
}

const renderMembers = (state) => {
  t.Object(state, ['state',])
  const {group,} = state
  t.Object(group, ['state.group',])
  if (isEmpty(group.members || [])) {
    return h(`#group-members.empty-group-tab`, `${group.name} doesn't have any members.`)
  }

  return h(`#group-members`, map((member) => {
    return h(`.group-member-container`, [
      h(`a`, {href: `/u/${member.username}`,}, h(`.group-member-avatar-container`,
        h(`img.group-member-avatar`, {src: member.avatarUrl,}))),
      h(`.group-member-content`, [
        h(`a`, {href: `/u/${member.username}`,}, [
          h(`.group-member-name`, `${member.name} (${member.username})`),
          h(`.group-member-bio`, member.bio),
        ]),
      ]),
    ])
  }, group.members))
}

const renderLeftColumn = (state, group) => {
  t.Object(state, ['state',])
  t.Object(group, ['group',])
  const creationDate = DateTime.fromISO(group.added).toLocal().toLocaleString(DateTime.DATE_FULL)
  const avatarElem = h(`#avatar-container`)
  avatarElem.innerHTML = jdenticon.toSvg(group.path, 228)
  const numMembers = group.members.length
  const membersSfx = numMembers !== 1 ? 's' : ''
  return h(`#left-column`, [
    avatarElem,
    h(`#group-names-container.bb.b--light-gray.pv3.mb0`, [
      h(`#group-name`, group.name),
      h(`#group-path.muted`, group.path),
    ]),
    h(`#group-metadata-container.b--light-gray.pv3.mb0.f6`, [
      h(`#group-description.group-metadata.pt1`, group.description),
      h(`#group-creation-date.group-metadata.pt1`, `Created ${creationDate}`),
      h(`#group-num-members.group-metadata.pt1`, `Has ${numMembers} member${membersSfx}`),
    ]),
  ])
}

const renderRightColumn = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const {group,} = state
  t.Object(group, ['state.group',])
  let renderer
  let activeIndex
  if (state.activeTab === `upcoming`) {
    renderer = renderUpcoming
    activeIndex = 0
  } else if (state.activeTab === `past`) {
    renderer = renderPast
    activeIndex = 1
  } else if (state.activeTab === `members`) {
    renderer = renderMembers
    activeIndex = 2
  } else {
    throw new Error(`Unrecognized tab '${state.activeTab}'`)
  }
  return h(`#right-column`, [
    h(`#top-section`, [
      h(`nav#group-info-tabs.bg-white`, h(`ul.pa0.ma0.list.flex.bg-white`, addIndex(map)((name, i) => {
        logger.debug(`i: ${i}, name: ${name}`)
        t.Number(i, ['i',])
        t.String(name, ['name',])
        let sfx = ''
        if (i === activeIndex) {
          sfx = `.active`
        }
        return h(`li.tab${sfx}`, {
          onclick: () => {
            if (state.activeTab !== name) {
              logger.debug(`Switching to group ${name} tab`)
              state.activeTab = name
              emit.triggerRender()
            }
          },
        }, titleize(name))
      }, ['upcoming', 'past', 'members',]))),
      h(`#group-toolbar`, [
        h(`a#new-meet-button.button.primary-button`, {href: `/g/${group.path}/new-meet`,}, `New Meet`),
      ]),
    ]),
    h(`#group-info-tab-content.ba.b--light-gray.bt-0`, [
      renderer(state),
    ]),
  ])
}

module.exports = {
  name: 'groupProfileView',
  route: '/g/:path',
  resetState: (state) => {
    state.activeTab = `upcoming`
  },
  loadData: async (state, emit) => {
    t.Object(state, ['state',])
    t.Function(emit, ['emit',])
    const path = state.params.path
    assert.ok(!isNullOrBlank(path))
    logger.debug(`Loading group ${path}...`)
    const group = await ajax.getJson(`/api/groups/${path}`)
    logger.debug(`Successfully loaded group '${path}'`)
    state.group = group
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])
    const {group,} = state
    t.Object(group, ['state.group',])
    logger.debug(`Rendering profile page of group '${group.path}'`, {state,})
    return h(`.${sheet}`, h(`#group-profile-view`, [
      renderLeftColumn(state, group),
      renderRightColumn(state, emit, group),
    ]))
  },
}

const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const Promise = require('bluebird')

const logger = require('../logger').child({module: 'views/notFound',})
const sheet = sheetify('./notFound.styl')

// This view defines a component acting on its component state
module.exports = {
  name: 'notFoundView',
  route: '*',
  resetState: (state) => {
    state.redirectIn = null
  },
  render: (state, emit) => {
    if (state.redirectIn == null) {
      state.redirectIn = 3
      emit('redirectIn', state.redirectIn)
    } else {
      t.Number(state.redirectIn, ['redirectIn',])
    }
    const suffix = state.redirectIn !== 1 ? 's' : ''
    return h(`.${sheet}`, [
      h('#not-found-view',
        `Nothing to see here, redirecting in ${state.redirectIn} second${suffix}...`),
    ])
  },
  initialize: (state, emitter) => {
    state.redirectIn = null

    emitter.on('redirectIn', async (seconds) => {
      state.redirectIn = seconds
      while (state.redirectIn != null && state.redirectIn > 0) {
        logger.debug(`Redirecting in ${state.redirectIn} second(s)...`)
        await Promise.delay(1000)
        --state.redirectIn
        emitter.triggerRender()
      }

      logger.debug(`Redirecting to /`)
      emitter.emitGlobal(state.events.PUSHSTATE, '/')
    })
  },
}

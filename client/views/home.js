const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')

const logger = require('../logger').child({module: 'views/home',})

const sheet = sheetify('./home.styl')

module.exports = {
  name: 'homeView',
  route: '/',
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])

    logger.debug(`Rendering`)
    return h(`.${sheet}`, h('p', `Nothing to see here`))
  },
}

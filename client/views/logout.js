const t = require('tcomb')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')

const loadingComponent = require('../components/loading')(['logoutView', 'loading',])
const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/logout',})

const logUserOut = async () => {
    await ajax.postJson('/api/logout')
}

module.exports = {
  name: 'logoutView',
  route: '/logout',
  resetState: (state) => {
    logger.debug(`Resetting state`)
    state.input = {
      usernameOrEmail: '',
      password: '',
    }
    state.formErrorMessage = null
    state.isLoading = false
    state.submissionEnabled = false
    state.errorNotification = null
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])

    logger.debug(`Logging user out...`)
    state.isLoading = true
    logUserOut().then(() => {
      logger.debug(`User logout succeeded`)
      state.global.loggedInUser = null
      redirect(state, emit)
    }, (err) => {
      logger.error(`User logout failed`, {err,})
      redirect(state, emit)
    })

    return loadingComponent.render(state, emit)
  },
}

const redirect = (state, emit) => {
  state.isLoading = false
  emit.triggerRender()

  if (!isNullOrBlank(state.query.next)) {
    const nextUrl = decodeURIComponent(state.query.next)
    logger.debug(`Redirecting to ${nextUrl}`)
    window.location.href = nextUrl
  } else {
    logger.debug(`Redirecting to homepage`)
    emit.global('pushState', '/')
  }
}

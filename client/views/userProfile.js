const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const assert = require('assert')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const {DateTime,} = require('luxon')
const map = require('ramda/src/map')
const addIndex = require('ramda/src/addIndex')
const titleize = require('titleize')
const jdenticon = require('jdenticon')
const Promise = require('bluebird')

const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/userProfile',})

const sheet = sheetify('./userProfile.styl')

const renderLeftColumn = (state, user) => {
  t.Object(state, ['state',])
  t.Object(user, ['user',])
  t.Array(state.userGroups, ['state.userGroups',])
  t.Array(state.pastMeets, ['state.pastMeets',])
  const joinDate = DateTime.fromISO(user.added).toLocal().toLocaleString(DateTime.DATE_FULL)
  const numGroups = state.userGroups.length
  const groupsSfx = numGroups !== 1 ? `s` : ''
  const numPrevMeets = state.pastMeets.length

  return h(`#left-column`, [
    h(`img#user-avatar`, {src: user.avatarUrl,}),
    h(`#user-names-container.bb.b--light-gray.pv3.mb0`, [
      h(`#user-name`, user.name),
      h(`#user-username.muted`, state.params.username),
    ]),
    h(`#user-metadata-container.b--light-gray.pv3.mb0.f6`, [
      h(`#user-bio.user-metadata.pt1`, user.bio),
      h(`#user-join-date.user-metadata.pt1`, `Joined ${joinDate}`),
      h(`#user-num-meets-gone-to.user-metadata.pt1`, `Has gone to ${numPrevMeets} meet${
        numPrevMeets != 1 ? 's' : ''}`),
      h(`#user-num-groups.user-metadata.pt1`, `Member of ${numGroups} group${groupsSfx}`),
    ]),
  ])
}

const renderUpcomingMeets = (state) => {
  t.Object(state, ['state',])
  const {user,} = state
  t.Object(user, ['user',])
  const {loggedInUser,} = state.global
  const isLoggedInUser = loggedInUser != null && user.username === loggedInUser.username
  const meets = state.upcomingMeets || []
  if (meets.length === 0) {
    return h(`.empty-user-info-tab-content`, isLoggedInUser ? `You don't have any upcoming meets.` :
      `${user.name} doesn't have any upcoming meets.`)
  }

  return h(`#upcoming-meets`, map((meet) => {
    t.Object(meet, ['meet',])
    const startTime = DateTime.fromISO(meet.starts).toLocal()
    const endTime = DateTime.fromISO(meet.ends).toLocal()
    const endDateAndTimeStr = !endTime.hasSame(startTime, 'day') ? endTime.toLocaleString({
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: '2-digit',
    }) : endTime.toLocaleString({
      hour: 'numeric',
      minute: '2-digit',
    })
    const meetTime = `${startTime.toLocaleString({
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: '2-digit',
    })} - ${endDateAndTimeStr}`
    return h(`.individual-meet`, h(`.meet-container`, h(`.meet-inner`, [
      h(`.meet-content`, [
        h(`a`, {href: `/m/${meet.uuid}`,}, [
          h(`.meet-title`, meet.title),
          h(`.meet-time`, meetTime),
          h(`.meet-location`, meet.locationName),
        ]),
      ]),
    ])))
  }, meets))
}

const renderPastMeets = (state) => {
  t.Object(state, ['state',])
  const {user,} = state
  t.Object(user, ['user',])
  const {loggedInUser,} = state.global
  const isLoggedInUser = loggedInUser != null && user.username === loggedInUser.username
  const meets = state.pastMeets || []
  if (meets.length === 0) {
    return h(`.empty-user-info-tab-content`, isLoggedInUser ? `You haven't gone to any meets yet.` :
      `${user.name} hasn't gone to any meets.`)
  }

  return h(`#past-meets`, map((meet) => {
    t.Object(meet, ['meet',])
    const startTime = DateTime.fromISO(meet.starts).toLocal()
    const endTime = DateTime.fromISO(meet.ends).toLocal()
    const endDateAndTimeStr = !endTime.hasSame(startTime, 'day') ? endTime.toLocaleString({
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: '2-digit',
    }) : endTime.toLocaleString({
      hour: 'numeric',
      minute: '2-digit',
    })
    const meetTime = `${startTime.toLocaleString({
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: '2-digit',
    })} - ${endDateAndTimeStr}`
    return h(`.individual-meet`, h(`.meet-container`, h(`.meet-inner`, [
      h(`.meet-content`, [
        h(`a`, {href: `/m/${meet.uuid}`,}, [
          h(`.meet-title`, meet.title),
          h(`.meet-time`, meetTime),
          h(`.meet-location`, meet.locationName),
        ]),
      ]),
    ])))
  }, meets))
}

const renderUserActivity = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  let renderer
  let activeIndex
  if (state.activeActivityTab === `upcoming`) {
    renderer = renderUpcomingMeets
    activeIndex = 0
  } else if (state.activeActivityTab === `past`) {
    renderer = renderPastMeets
    activeIndex = 1
  } else {
    throw new Error(`Unrecognized tab '${state.activeActivityTab}'`)
  }
  return h(`#user-activity`, [
    h(`#user-activity-header.eventfeed-header`, [
      h(`nav`, h(`ul`, addIndex(map)((name, i) => {
        t.String(name, ['name',])
        t.Number(i)
        let sfx = ''
        if (i === activeIndex) {
          sfx = `.active`
        }
        return h(`li${sfx}`, {
          onclick: () => {
            if (state.activeTab !== name) {
              logger.debug(`Switching to ${name} meets tab`)
              state.activeActivityTab = name
              emit.triggerRender()
            }
          },
        }, titleize(name))
      }, [`upcoming`, `past`,]))),
    ]),
    h(`#user-activity-content`, renderer(state)),
  ])
}

const renderUserGroups = (state) => {
  t.Object(state, ['state',])
  const {user,} = state
  t.Object(user, ['state.user',])
  const groups = state.userGroups || []
  if (groups.length === 0) {
    return h(`#user-groups.empty-user-info-tab-content`, `${user.name} isn't in any groups.`)
  }

  return h(`#user-groups`, map((group) => {
    const avatarElem = h(`.group-avatar-container`)
    avatarElem.innerHTML = jdenticon.toSvg(group.path, 80)
    return h(`.group-container`, h(`.group-inner`, [
      h(`a`, {href: `/g/${group.path}`,}, avatarElem),
      h(`.group-content`, [
        h(`a`, {href: `/g/${group.path}`,}, [
          h(`.group-name`, group.name),
          h(`.group-description`, group.description),
        ]),
      ]),
    ]))
  }, groups))
}

const renderRightColumn = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  let renderer
  let activeIndex
  if (state.activeTab === `activity`) {
    renderer = renderUserActivity
    activeIndex = 0
  } else if (state.activeTab === `groups`) {
    renderer = renderUserGroups
    activeIndex = 1
  } else {
    throw new Error(`Unrecognized tab '${state.activeTab}'`)
  }
  return h(`#right-column`, [
    // We need a container for the tab elements, so we can make it 100% wide and cover any elements
    // scrolling beneath it
    h(`nav#user-info-tabs.bg-white`, h(`ul.pa0.ma0.list.flex.bg-white.bb.b--light-gray.w-100`, addIndex(map)((name, i) => {
      t.String(name, ['name',])
      t.Number(i, ['i',])
      let sfx = ''
      if (i === activeIndex) {
        sfx = `.active`
      }
      return h(`li.tab${sfx}`, {
        onclick: () => {
          if (state.activeTab !== name) {
            logger.debug(`Switching to user ${name} tab`)
            state.activeTab = name
            emit.triggerRender()
          }
        },
      }, titleize(name))
    }, ['activity', 'groups',]))),
    h(`#user-info-tab-content.ba.b--light-gray.bt-0`, [
      renderer(state, emit),
    ]),
  ])
}

module.exports = {
  name: 'userProfileView',
  route: '/u/:username',
  resetState: (state) => {
    state.activeTab = `activity`
    state.activeActivityTab = `upcoming`
  },
  loadData: async (state, emit) => {
    t.Object(state, ['state',])
    t.Function(emit, ['emit',])
    const username = state.params.username
    assert.ok(!isNullOrBlank(username))
    logger.debug(`Loading user ${username}...`)
    const [user, groups, upcomingMeets, pastMeets,] = await Promise.all([
      ajax.getJson(`/api/users/${username}`),
      ajax.getJson(`/api/users/${username}/groups`),
      ajax.getJson(`/api/users/${username}/upcoming-meets`),
      ajax.getJson(`/api/users/${username}/past-meets`),
    ])
    logger.debug(`Successfully loaded user ${username}`)
    state.user = user
    state.userGroups = groups
    state.upcomingMeets = upcomingMeets
    state.pastMeets = pastMeets
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])
    const {user,} = state
    t.Object(user, ['state.user',])
    logger.debug(`Rendering profile page of user '${state.params.username}'`, {state,})
    return h(`.${sheet}`, [
      renderLeftColumn(state, user),
      renderRightColumn(state, emit, user),
    ])
  },
}
